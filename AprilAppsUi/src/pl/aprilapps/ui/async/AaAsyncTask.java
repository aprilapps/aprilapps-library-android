package pl.aprilapps.ui.async;

import pl.aprilapps.ui.activities.AaActionBarActivity;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;

public abstract class AaAsyncTask<Params, Progress, Result> extends AsyncTask<Params, Progress, Result> {
	
	private boolean finished;
	private boolean activityListener;;
	
	public interface IAaTask {
		public void onDownloaderStarted();
		public void onDownloaderFinished();
	}		
	
	private IAaTask listener;
	
	public AaAsyncTask(IAaTask listener) {
		this.listener = listener;
		if (listener instanceof AaActionBarActivity) activityListener = true;
		else activityListener = false; 
	}
	
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		try {
			listener.onDownloaderStarted();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected Result doInBackground(Params... params) {
		return null;
	};
	
	protected void onPostExecute(Result result) {
		super.onPostExecute(result);
		try {
			if (isCancelled()) return;
			listener.onDownloaderFinished();
		} catch (Exception e) {
			e.printStackTrace();
		}
	};

	public void setListener(IAaTask listener) {
		this.listener = listener;	
	}
	
	public void detach() {
		onDetach();		
	}		
	
	public void onDetach() {
		this.listener = null;
	}
	
	public IAaTask getBaseListener() {
		return listener;
	}

	public boolean isFinished() {
		return finished;
	}

	public void setFinished(boolean finished) {
		this.finished = finished;
	}

	public IAaTask getListener() {
		return listener;
	}
	
	protected AaActionBarActivity getActivity() {
		if (activityListener) return (AaActionBarActivity) getListener();
		else {
			Fragment fragment = (Fragment) getListener(); 
			return (AaActionBarActivity) fragment.getActivity();
		}
	}
	
	public void forceCancel() {
		this.cancel(true);
	}
	
}
