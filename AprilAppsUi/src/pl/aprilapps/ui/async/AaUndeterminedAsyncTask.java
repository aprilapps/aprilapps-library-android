package pl.aprilapps.ui.async;

import pl.aprilapps.ui.dialogs.AaDialogFragment;

public abstract class AaUndeterminedAsyncTask<Params, Progress, Result> extends AaAsyncTask<Params, Progress, Result> {		
	
	protected AaDialogFragment progressDialog;
	
	public AaUndeterminedAsyncTask(IAaTask listener) {
		super(listener);
	}
	
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
	}

	protected Result doInBackground(Params... params) {
		return null;
	};
	
	protected void onPostExecute(Result result) {
		super.onPostExecute(result);
		setFinished(true);
		try {
			if (progressDialog != null) progressDialog.dismiss();
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}

	public void setProgressDialog(AaDialogFragment progressDialog) {
		this.progressDialog = progressDialog;
	}

	public AaDialogFragment getProgressDialog() {
		return progressDialog;
	};	
	
	@Override
	public void forceCancel() {
		super.forceCancel();
		try {
			if (progressDialog != null) progressDialog.dismiss();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

}
