package pl.aprilapps.ui.dialogs;

import pl.aprilapps.ui.AaApplication;
import pl.aprilapps.ui.activities.AaActionBarActivity;
import android.support.v4.app.DialogFragment;

public class AaDialogFragment extends DialogFragment {
	
	public AaActionBarActivity getAprilAppsActivity() {
		return (AaActionBarActivity) getActivity();
	}
	
	public AaApplication getApplication() {
		return getAprilAppsActivity().getApp();
	}
	

}
