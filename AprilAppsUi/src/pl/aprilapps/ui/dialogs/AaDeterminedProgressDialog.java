package pl.aprilapps.ui.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.widget.ProgressBar;
import android.widget.TextView;

public class AaDeterminedProgressDialog extends AaDialogFragment {		
	
	public final static String TAG = "DeterminedProgressDialog";

	public final static String KEY_MAX = "max";
	public final static String KEY_PROGRESS = "progress";
	
	protected int max;
	protected int progress;
	protected ProgressBar progressBar;
	protected TextView message;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle args = getArguments();
		if (args != null) {
			max = args.getInt(KEY_MAX);
			progress = args.getInt(KEY_PROGRESS);
		}
	}
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		
		if (savedInstanceState != null) {
			max = savedInstanceState.getInt(KEY_MAX);
			progress = savedInstanceState.getInt(KEY_PROGRESS);
		}
		
		return super.onCreateDialog(savedInstanceState);
	}

	@Override
	public void onSaveInstanceState(Bundle args) {
		args.putInt(KEY_MAX, max);
		args.putInt(KEY_PROGRESS, progress);
		super.onSaveInstanceState(args);
	}
	
	public void setMax(int max) {
		this.max = max;
		progressBar.setMax(max);
	}
	
	public void updateProgress(int operation) {
		progress ++;
		progressBar.setProgress(progress);

	}

}
