package pl.aprilapps.ui.dialogs;

import pl.aprilapps.ui.R;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

public class AaUndeterminedProgressDialog extends AaDialogFragment {		
	
	public final static String TAG = "UndeterminedProgressDialog";	
	public final static String KEY_TITLE = "title";	
	public final static String KEY_MESSAGE = "message";	

	protected ProgressBar progressBar;
	protected TextView messageView;
	protected String title;
	protected String message;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle args = getArguments();
		if (savedInstanceState == null && args != null) {
			title = args.getString(KEY_TITLE);
			message = args.getString(KEY_MESSAGE);	
		} else if (savedInstanceState != null) {
			title = savedInstanceState.getString(KEY_TITLE);
			message = savedInstanceState.getString(KEY_MESSAGE);	
		}
		if (title == null) title = getString(R.string.loading_title);
		if (message == null) message = getString(R.string.loading_message);
	}
	
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		super.onCreateDialog(savedInstanceState);
		LayoutInflater inflater = getActivity().getLayoutInflater();
		ViewGroup view = (ViewGroup) inflater.inflate(R.layout.dialog_undetermined_progress, null);
		progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);	
		messageView = (TextView) view.findViewById(R.id.message);
		messageView.setText(message);
		
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle(title).setView(view);
		
		return builder.create(); 
	}

	@Override
	public void onSaveInstanceState(Bundle args) {
		args.putString(KEY_TITLE, title);
		args.putString(KEY_MESSAGE, message);
		super.onSaveInstanceState(args);
	}

}
