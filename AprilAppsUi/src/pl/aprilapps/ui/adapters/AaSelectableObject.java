package pl.aprilapps.ui.adapters;

import java.io.Serializable;

public class AaSelectableObject implements Serializable{

	private static final long serialVersionUID = 570985782751319827L;

	private boolean selected;

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	
	
}
