package pl.aprilapps.ui.activities;

import pl.aprilapps.ui.R;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;

/**
 * @author Jacek
 * Instrukcja implementacji:
 * 1. Zasilamy elementami tablice drawerItemTitles, drawerItemSubtitles i draweItemIcons
 * 2. Tworzymy instancje dziedziczaca po BaseAdapter i wstawiamy do zmiennej menuAdapter
 * 3. Ustawiamy liscie drawerList nowo utworzony adapter (setAdapter())
 * 4. Tworzymy instancje ActionBarDrawerToggle
 * 5. Ustawiamy dla drawerLayout listener (nowo utworzony ActionBarDrawerToggle)
 * 6. Nadpisujemy selectItem (dajemy switch na poczatku metody i konczymy wywolaniem super-metody)
 */
public class AaDrawerActivity extends AaActionBarActivity {
	
    public static final int MORE_SETTINGS = -20;

	protected DrawerLayout drawerLayout;
    protected ListView drawerList;
    protected ActionBarDrawerToggle drawerToggle;
    protected BaseAdapter menuAdapter;
    protected String[] drawerItemTitles;
    protected String[] drawerItemSubtitles;
    protected int[] drawerItemIcons;

    private boolean adsEnabled;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer);
 
//      //TODO Implementacja docelowa
//        // Generate title
//        title = new String[] { "Title Fragment 1", "Title Fragment 2",
//                "Title Fragment 3" };
// 
//        // Generate subtitle
//        subtitle = new String[] { "Subtitle Fragment 1", "Subtitle Fragment 2",
//                "Subtitle Fragment 3" };
// 
//        // Generate icon
//        icon = new int[] { R.drawable.action_search_dark, R.drawable.action_about_dark,
//                R.drawable.content_save_dark };
 
        // Locate DrawerLayout in drawer_main.xml
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
 
        // Locate ListView in drawer_main.xml
        drawerList = (ListView) findViewById(R.id.left_drawer);
 
        // Set a custom shadow that overlays the main content when the drawer
        // opens
        drawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
 
//        //TODO Implementacja docelowa
//        // Pass results to MenuListAdapter Class
//        menuAdapter = new MenuListAdapter(this, title, subtitle, icon);
// 
//        // Set the MenuListAdapter to the ListView
//        drawerList.setAdapter(menuAdapter);
 
        // Capture button clicks on side menu
        drawerList.setOnItemClickListener(new DrawerItemClickListener());
 
        // Enable ActionBar app icon to behave as action to toggle nav drawer
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
 
//      //TODO Implementacja docelowa
//        // ActionBarDrawerToggle ties together the the proper interactions
//        // between the sliding drawer and the action bar app icon
//        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.drawable.ic_drawer, R.string.drawer_open,
//                R.string.drawer_close) {
// 
//            public void onDrawerClosed(View view) {
//                // TODO Auto-generated method stub
//                super.onDrawerClosed(view);
//            }
// 
//            public void onDrawerOpened(View drawerView) {
//                // TODO Auto-generated method stub
//                super.onDrawerClosed(drawerView);
//            }
//        };
// 
//        drawerLayout.setDrawerListener(drawerToggle);
 
        if (savedInstanceState == null) {
            selectItem(0);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getSupportMenuInflater().inflate(R.menu.drawer_menu, menu);
        return true;
    }
 
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
 
        if (!isCustomDrawer() && item.getItemId() == android.R.id.home) {
 
            if (drawerLayout.isDrawerOpen(drawerList)) {
                drawerLayout.closeDrawer(drawerList);
            } else {
                drawerLayout.openDrawer(drawerList);
            }
        }
 
        return super.onOptionsItemSelected(item);
    }
 
    public boolean isCustomDrawer() {
    	return false;
    }
    
    // The click listener for ListView in the navigation drawer
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }
    }
    
    
    protected void selectItem(int position) {
 
//    	// TODO Implementacja docelowa
//        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
//        // Locate Position
//        switch (position) {
//        case 0:
//            ft.replace(R.id.content_frame, fragment1);
//            break;
//        case 1:
//            ft.replace(R.id.content_frame, fragment2);
//            break;
//        case 2:
//            ft.replace(R.id.content_frame, fragment3);
//            break;
//        }
//        ft.commit();
        
        drawerList.setItemChecked(position, true);
        // Close drawer
        drawerLayout.closeDrawer(drawerList);
    }
 
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        drawerToggle.syncState();
    }
 
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggles
        drawerToggle.onConfigurationChanged(newConfig);
    }

    public boolean isAdsEnabled() {
        return adsEnabled;
    }

    public void setAdsEnabled(boolean adsEnabled) {
        this.adsEnabled = adsEnabled;
    }

    public DrawerLayout getDrawerLayout() {
        return drawerLayout;
    }

    public void setDrawerItemSubtitles(String[] drawerItemSubtitles) {
        this.drawerItemSubtitles = drawerItemSubtitles;
    }

    public void setDrawerItemTitles(String[] drawerItemTitles) {
        this.drawerItemTitles = drawerItemTitles;
    }

    public void setDrawerItemIcons(int[] drawerItemIcons) {
        this.drawerItemIcons = drawerItemIcons;
    }

    public void setEnuAdapter(BaseAdapter enuAdapter) {
        menuAdapter = enuAdapter;
    }

    public void setDrawerToggle(ActionBarDrawerToggle drawerToggle) {
        this.drawerToggle = drawerToggle;
    }

    public ListView getDrawerList() {
        return drawerList;
    }

}
