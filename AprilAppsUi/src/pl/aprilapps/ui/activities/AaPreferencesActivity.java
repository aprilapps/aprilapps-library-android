package pl.aprilapps.ui.activities;

import pl.aprilapps.ui.AaApplication;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.view.LayoutInflater;

public abstract class AaPreferencesActivity extends PreferenceActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getApp().registerActivity(this);
		super.onCreate(savedInstanceState);
	}
	
	public void destroy() {
		getApp().unregisterActivity(this);
		finish(); 
	} 
	
	public LayoutInflater getInflater() {
		return (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
	}
	
	public AaApplication getApp() {
		return (AaApplication) getApplication();
	}
}
