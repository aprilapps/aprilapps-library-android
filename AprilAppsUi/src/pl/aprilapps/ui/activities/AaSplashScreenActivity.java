package pl.aprilapps.ui.activities;

import pl.aprilapps.ui.AaApplication;
import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;

public class AaSplashScreenActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getApp().registerActivity(this);
		super.onCreate(savedInstanceState);
	}
	
	public LayoutInflater getInflater() {
		return (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
	}
	
	@Override
	protected void onDestroy() {
		getApp().unregisterActivity(this);
		super.onDestroy();
	}
		
	@Override
	protected void onResume() {
		super.onResume();				
	}
	
	@Override
	protected void onPause() {
		super.onPause();
	}
	
	public AaApplication getApp() {
		return (AaApplication) getApplication();
	}
}
