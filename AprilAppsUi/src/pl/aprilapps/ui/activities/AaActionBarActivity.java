package pl.aprilapps.ui.activities;

import pl.aprilapps.ui.AaApplication;
import pl.aprilapps.ui.async.AaAsyncTask;
import pl.aprilapps.ui.async.AaAsyncTask.IAaTask;
import pl.aprilapps.ui.async.AaDeterminedAsyncTask;
import pl.aprilapps.ui.async.AaUndeterminedAsyncTask;
import pl.aprilapps.ui.dialogs.AaDeterminedProgressDialog;
import pl.aprilapps.ui.dialogs.AaDialogFragment;
import pl.aprilapps.ui.dialogs.AaUndeterminedProgressDialog;
import pl.aprilapps.ui.fragments.AaFragment;
import android.annotation.SuppressLint;
import android.content.res.Configuration;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.Window;

public class AaActionBarActivity extends ActionBarActivity implements IAaTask {

	protected AaAsyncTask<?, ?, ?> downloader;
	private AaDialogFragment progressDialog;
	private boolean visible;

	@SuppressWarnings("rawtypes")
	@Override
	protected void onCreate(Bundle savedInstanceState) {		
		getApp().registerActivity(this);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		requestWindowFeature(Window.FEATURE_PROGRESS);
		super.onCreate(savedInstanceState);

		if (getApp().isRemovedFromMemory())	return;

		downloader = (AaAsyncTask<?, ?, ?>) getLastCustomNonConfigurationInstance();
		if (downloader != null) downloader.setListener(this);

		if (savedInstanceState != null) {
			AaUndeterminedProgressDialog undeterminedProgress = (AaUndeterminedProgressDialog) getSupportFragmentManager()
					.findFragmentByTag(AaUndeterminedProgressDialog.TAG);
			AaDeterminedProgressDialog determinedProgress = (AaDeterminedProgressDialog) getSupportFragmentManager()
					.findFragmentByTag(AaDeterminedProgressDialog.TAG);

			if (undeterminedProgress != null && downloader != null && downloader instanceof AaUndeterminedAsyncTask) {
				AaUndeterminedAsyncTask undeterminedTask = (AaUndeterminedAsyncTask) downloader;
				progressDialog = undeterminedProgress;
				undeterminedTask.setProgressDialog(undeterminedProgress);
				if (undeterminedTask.isFinished())
					undeterminedProgress.dismiss();
			} else if (determinedProgress != null && downloader != null && downloader instanceof AaDeterminedAsyncTask) {
				AaDeterminedAsyncTask determinedTask = (AaDeterminedAsyncTask) downloader;
				progressDialog = undeterminedProgress;
				determinedTask.setProgressDialog(determinedProgress);
				if (determinedTask.isFinished())
					determinedProgress.dismiss();
			}
		}
	}

	public AaAsyncTask<?, ?, ?> getDownloader() {
		return downloader;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void onDownloaderStarted() {
		if (downloader == null)
			return;

		if (downloader instanceof AaDeterminedAsyncTask) {
			AaDeterminedAsyncTask determinedTask = (AaDeterminedAsyncTask) downloader;
			this.progressDialog = determinedTask.getProgressDialog();
		} else if (downloader instanceof AaUndeterminedAsyncTask) {
			AaUndeterminedAsyncTask indeterminedTask = (AaUndeterminedAsyncTask) downloader;
			this.progressDialog = indeterminedTask.getProgressDialog();
		}
	}

	public void setDownloader(AaAsyncTask<?, ?, ?> downloader) {
//		Log.v(AaApplication.TAG_LOG, "setDownloader");
		if (this.downloader != null) this.downloader.cancel(true);
		this.downloader = downloader;
	}

	@Override
	public Object onRetainCustomNonConfigurationInstance() {
		if (downloader != null)	downloader.detach();
		return downloader;
	}

	@Override
	protected void onResume() {
		super.onResume();		
		if (getApp().isRemovedFromMemory()) return;
		setSupportProgressBarIndeterminateVisibility(false);
		visible = true;

		try {
			if (progressDialog != null && downloader == null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		visible = false;
	}

	public void injectInnerFragment(AaFragment fragment, int viewHolderId, boolean addToBackStack, boolean replace) {
		FragmentManager manager = getSupportFragmentManager();
		FragmentTransaction transaction = manager.beginTransaction();
		
		if (fragment.isAdded() && !replace) {
			transaction.attach(fragment);
		} else if (replace) {
			transaction.replace(viewHolderId, fragment, fragment.getClass().getSimpleName());
			if (addToBackStack)	transaction.addToBackStack(null);
		} else {
			transaction.add(viewHolderId, fragment, fragment.getClass().getSimpleName());
			if (addToBackStack)	transaction.addToBackStack(null);
		}	
		try {
			transaction.commitAllowingStateLoss();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void injectInnerFragment(AaFragment fragment, int viewHolderId, int showAnimation, int hideAnimation, boolean addToBackStack, boolean replace) {
		FragmentManager manager = getSupportFragmentManager();
		FragmentTransaction transaction = manager.beginTransaction();
		transaction.setCustomAnimations(showAnimation, hideAnimation);

		if (fragment.isAdded() && !replace) {
			transaction.attach(fragment);
		} else if (replace) {
			transaction.replace(viewHolderId, fragment, fragment.getClass().getSimpleName());
			if (addToBackStack)	transaction.addToBackStack(null);
		}
		else {
			transaction.add(viewHolderId, fragment, fragment.getClass().getSimpleName());
			if (addToBackStack)	transaction.addToBackStack(null);
		}
		try {
			transaction.commitAllowingStateLoss();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onDownloaderFinished() {
		downloader = null;
	}

	@SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
	public int getScreenHeight() {
		Display display = getWindowManager().getDefaultDisplay();
		if (AaApplication.isAtLeastApiLevel(13)) {
			Point size = new Point();
			display.getSize(size);
			int height = size.y;
			return height;
		} else {
			int height = display.getHeight(); // deprecated
			return height;
		}
	}

	@SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
	public int getScreenWidth() {
		Display display = getWindowManager().getDefaultDisplay();
		if (AaApplication.isAtLeastApiLevel(13)) {
			Point size = new Point();
			display.getSize(size);
			int width = size.x;
			return width;
		} else {
			int width = display.getWidth();
			return width;
		}
	}

	public AaApplication getApp() {
		return (AaApplication) getApplication();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			int backStackCount = getSupportFragmentManager().getBackStackEntryCount();
			if (backStackCount <= 1) onHomePressed();
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void onHomePressed() {
		finish();
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		visible = false;
	}

	public boolean isVisible() {
		return visible;
	}
	
	@Override
	protected void onDestroy() {
		getApp().unregisterActivity(this);
		if (isDownloading()) getDownloader().cancel(true);
		super.onDestroy();
	}
	
	public boolean isDownloading() {
//		Log.v(AaApplication.TAG_LOG, "isDownloading");
		return (getDownloader() != null);
	}

	public AaDialogFragment getProgressDialog() {
		return progressDialog;
	}

	public void setProgressDialog(AaDialogFragment progressDialog) {
		this.progressDialog = progressDialog;
	}
	
    @SuppressLint("NewApi")
	public boolean isTablet() {
        if (Build.VERSION.SDK_INT < 11) return false;

        Configuration config = getResources().getConfiguration();
        try {
            return (config.smallestScreenWidthDp >= 600);
        } catch (Exception e) {
            return false;
        }
    }
	
}
