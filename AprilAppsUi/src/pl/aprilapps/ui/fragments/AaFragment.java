package pl.aprilapps.ui.fragments;

import pl.aprilapps.ui.AaApplication;
import pl.aprilapps.ui.activities.AaActionBarActivity;
import pl.aprilapps.ui.async.AaAsyncTask;
import pl.aprilapps.ui.async.AaAsyncTask.IAaTask;
import pl.aprilapps.ui.async.AaDeterminedAsyncTask;
import pl.aprilapps.ui.async.AaUndeterminedAsyncTask;
import pl.aprilapps.ui.dialogs.AaDialogFragment;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;

public class AaFragment extends Fragment implements IAaTask{
	
	protected AaAsyncTask<?, ?, ?> downloader;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(false);
	}
		
	public AaActionBarActivity getAaActivity() {
		return (AaActionBarActivity) getActivity();
	}	

	
	public ActionBar getSupportActionBar() {
		return getAaActivity().getSupportActionBar();
	}
	
	public FragmentManager getSupportFragmentManager() {
		return getAaActivity().getSupportFragmentManager();
	}	
	
	public boolean isDownloading() {
		return (downloader != null);
	}

    public void injectNestedFragment(int container, AaFragment fragment, String tag) {
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.replace(container, fragment, tag);
        transaction.commit();
    }
    
    public AaApplication getApp() {
    	return (AaApplication) getActivity().getApplication();
    }
    
	public void setDownloader(AaAsyncTask<?, ?, ?> downloader) {
		getAaActivity().setDownloader(downloader);
		this.downloader = downloader;
	}
	
	public AaAsyncTask<?, ?, ?> getDownloader() {
		return downloader;
	}
	
	public AaDialogFragment getProgressDialog() {
		return getAaActivity().getProgressDialog();
	}
	
	public void setProgressDialog(AaDialogFragment dialog) {
		getAaActivity().setProgressDialog(dialog);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void onDownloaderStarted() {
		if (getDownloader() == null)
			return;

		if (getDownloader() instanceof AaDeterminedAsyncTask) {
			AaDeterminedAsyncTask determinedTask = (AaDeterminedAsyncTask) getDownloader();
			setProgressDialog(determinedTask.getProgressDialog());
		} else if (getDownloader() instanceof AaUndeterminedAsyncTask) {
			AaUndeterminedAsyncTask indeterminedTask = (AaUndeterminedAsyncTask) getDownloader();
			setProgressDialog(indeterminedTask.getProgressDialog());
		}
	}

	@Override
	public void onDownloaderFinished() {
		setDownloader(null);		
	}
	
	@Override
	public void onDetach() {
		if (downloader != null && downloader.equals(getDownloader())) downloader.forceCancel();
		super.onDetach();
	}
	
	public boolean isTablet() {
		return getAaActivity().isTablet();
	}
	
	public LayoutInflater getLayoutInflater() {
		return getAaActivity().getLayoutInflater();
	}
}
