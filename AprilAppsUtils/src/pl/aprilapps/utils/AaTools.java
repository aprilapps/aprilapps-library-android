package pl.aprilapps.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.io.IOUtils;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore.Images;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.ListView;

public class AaTools {

	public static String toString(ByteArrayInputStream is) {		
		try {
			return  IOUtils.toString(is, "UTF-8");
		} catch (IOException e) {
			return null;
		}
	}
	
	public static List<View> getCellsBelow(ListView listView, int position) {
		List<View> cells = new ArrayList<View>();		
		
		for (int i = position + 1; i <= listView.getLastVisiblePosition(); i++) {
			cells.add(listView.getChildAt(i));
		}
		
		return cells;
	}
	
	public static long[] toPrimitives(Long... objects) {
		if (objects == null) return null;
		
	    long[] primitives = new long[objects.length];
	    for (int i = 0; i < objects.length; i++)
	         primitives[i] = objects[i];

	    return primitives;
	}
	
	public static String getStringFromFile(File file) throws IOException {
		InputStream is = new FileInputStream(file);
		BufferedReader r = new BufferedReader(new InputStreamReader(is));
		StringBuilder total = new StringBuilder();
		String line;
		while ((line = r.readLine()) != null) {
		    total.append(line);
		}		
		return total.toString();
	}
	
	public static String getTableCreateScript(Context context, String tableName) throws IOException {
		AssetManager am = context.getAssets();		
		InputStream is = am.open("create_" + tableName + ".txt");
		BufferedReader r = new BufferedReader(new InputStreamReader(is));
		StringBuilder total = new StringBuilder();
		String line;
		while ((line = r.readLine()) != null) {
		    total.append(line);
		}		
		return total.toString();
	}
	
	public static Uri getImageUri(Context context, Bitmap inImage) {
		  String path = Images.Media.insertImage(context.getContentResolver(), inImage, "Title", null);
		  return Uri.parse(path);
	} 
	
	public static String formatTwoDigitsFloat(float number) {
		DecimalFormat df = new DecimalFormat("###,##");
		return df.format(number);
	}
	
	public static String formatTwoDigitsFloat(double number) {
		DecimalFormat df = new DecimalFormat("###,##");
		return df.format(number);
	}
	
	public static String formatOneDigitFloat(float number) {
		DecimalFormat df = new DecimalFormat("###,#");
		return df.format(number);
	}
	
	public static String formatOneDigitFloat(double number) {
		DecimalFormat df = new DecimalFormat("###,#");
		return df.format(number);
	}
	
	public static Bitmap bitmapFromUrl(String url) {
		try {
			URL req = new URL(url);
			Bitmap bitmap = BitmapFactory.decodeStream(req.openConnection().getInputStream());
			return bitmap;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static String[] getDeviceEmailAccounts(Context context){
		final List<String> emails = new ArrayList<String>();
		Pattern emailPattern = Patterns.EMAIL_ADDRESS; 
		Account[] accounts = AccountManager.get(context).getAccounts();
		for (Account account : accounts) {
		    if (emailPattern.matcher(account.name).matches()) {
		    	String email = account.name;
		    	if (!emails.contains(email))
		    		emails.add(account.name);
		    }
		}
		
		String[] emailsArray = new String[emails.size()];
		emailsArray = emails.toArray(emailsArray);
		return emailsArray;
	}
	
	
}
