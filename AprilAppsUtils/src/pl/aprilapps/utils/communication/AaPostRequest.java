package pl.aprilapps.utils.communication;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import org.apache.commons.io.Charsets;

import android.content.Context;

public class AaPostRequest<T> extends AaRequest<T> {
	
	private StringBuilder paramsBuilder;

	public AaPostRequest(String functionName, Context context) throws MalformedURLException {
		super(functionName, context);		
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void buildQuery() throws IOException, UnsupportedEncodingException {
		if (params.size() > 0) {
			paramsBuilder = new StringBuilder();
			Iterator it = params.entrySet().iterator();
			int i = 0;
		    while (it.hasNext()) {
		        Map.Entry<String, Object> pair = (Map.Entry)it.next();
		        Object value = pair.getValue();
		        
		        String param = value.toString();
		        String key = pair.getKey();
		        
		        if (i == 0) {
		        	paramsBuilder.append(key).append("=").append(URLEncoder.encode(param, "utf-8"));
		        } else {
		        	paramsBuilder.append("&").append(key).append("=").append(URLEncoder.encode(param, "utf-8"));
		        }      		        
		        i++;
		    }
		}
	}

	@Override
	public String getAPIUrl() {
		return null;
	}

	@Override
	public InputStream execute() throws Exception {
		try {
			buildQuery();
			   url = new URL(urlBuilder.toString()); 
			   if (useSSL()) connection = (HttpsURLConnection) url.openConnection();
			   else connection = (HttpURLConnection) url.openConnection();
			   connection.setConnectTimeout(getTimeout());
			   injectHeders();
			   
			   connection.setRequestMethod("POST");
			   if (paramsBuilder != null) {
			    connection.setDoOutput(true);
			    connection.setRequestProperty("Content-Length", "" + Integer.toString(paramsBuilder.toString().getBytes(Charsets.UTF_8).length));
			       
			    DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
			       wr.writeBytes(paramsBuilder.toString());
			       wr.flush();
			       wr.close();
			   }  
			   
			   statusCode = connection.getResponseCode();
			   
			   if (statusCode >= 200 && statusCode < 300) {
			    InputStream responseData = connection.getInputStream();
			    return responseData;
			   }   
			   return null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}	
	}
	
}
