package pl.aprilapps.utils;

import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang3.BooleanUtils;
import org.joda.time.DateTime;

import android.database.Cursor;

public class AaCursorTools {

	private Cursor cursor;

	public AaCursorTools(Cursor cursor) {
		super();
		this.cursor = cursor;
	}
	
	public String getString(String columnName) {
		return cursor.getString(cursor.getColumnIndex(columnName));
	}
	
	public int getInt(String columnName) {
		return cursor.getInt(cursor.getColumnIndex(columnName));
	}
	
	public long getLong(String columnName) {
		return cursor.getLong(cursor.getColumnIndex(columnName));
	}
	
	public float getFloat(String columnName) {
		return cursor.getFloat(cursor.getColumnIndex(columnName));
	}
	
	public double getDouble(String columnName) {
		return cursor.getDouble(cursor.getColumnIndex(columnName));
	}
	
	public boolean getBoolean(String columnName) {
		return BooleanUtils.toBoolean(cursor.getInt(cursor.getColumnIndex(columnName)));
	}
	
	public Date getDate(String columnName) {
		Calendar cal = AaDateTools.getDateFromSql(cursor.getString(cursor.getColumnIndex(columnName)));
		if (cal != null) return cal.getTime();
		else return null;
	}
	
	public Calendar getCalendar(String columnName) {
		return AaDateTools.getDateFromSql(cursor.getString(cursor.getColumnIndex(columnName)));
	}
	
	public DateTime getDateTime(String columnName) {
		Calendar cal = getCalendar(columnName);
		if (cal != null) return new DateTime(cal.getTimeInMillis());
		else return null;		
	}
	
	public byte[] getBlob(String columnName) {
		return cursor.getBlob(cursor.getColumnIndex(columnName));
	}
	
	//METODY STATYCZNE	
	public static String getString(Cursor cursor, String columnName) {
		return cursor.getString(cursor.getColumnIndex(columnName));
	}
	
	public static int getInt(Cursor cursor, String columnName) {
		return cursor.getInt(cursor.getColumnIndex(columnName));
	}
	
	public static long getLong(Cursor cursor, String columnName) {
		return cursor.getLong(cursor.getColumnIndex(columnName));
	}
	
	public static float getFloat(Cursor cursor, String columnName) {
		return cursor.getFloat(cursor.getColumnIndex(columnName));
	}
	
	public static double getDouble(Cursor cursor, String columnName) {
		return cursor.getDouble(cursor.getColumnIndex(columnName));
	}
	
	public static boolean getBoolean(Cursor cursor, String columnName) {
		return BooleanUtils.toBoolean(cursor.getInt(cursor.getColumnIndex(columnName)));
	}
	
	
}
