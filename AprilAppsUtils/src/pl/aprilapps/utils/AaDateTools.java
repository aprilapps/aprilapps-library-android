package pl.aprilapps.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDate;

import pl.aprilapps.utils.R;
import pl.aprilapps.utils.R.array;
import android.content.Context;

public class AaDateTools {
	
	
	/**
	 * 2012-01-01 12:30:45
	 */
	public static final String DATE_STANDARD = "yyyy-MM-dd HH:mm:ss"; 
	/**
	 * Grudzien 2012
	 */
	public static final String DATE_MONTH_YEAR = "MMM yyyy";
    /**
     * 12 Grudzien 2012
     */
    public static final String DATE_USER_FRIENDLY = "dd MMM yyyy";
	/**
	 * 2012-01-01
	 */
	public static final String DATE_STANDARD_SHORT = "yyyy-MM-dd";
	
	public static final String DATE_FOR_FILE_NAME_LONG = "yyyy-MM-dd HH_mm_ss"; 
	
	
	public final static double AVERAGE_MILLIS_PER_MONTH = 365.24 * 24 * 60 * 60 * 1000 / 12;

	public static Calendar getDateFromSql(String date) {
		SimpleDateFormat df = new SimpleDateFormat(DATE_STANDARD, Locale.getDefault());
		df.setTimeZone(TimeZone.getTimeZone("UTC"));
		Calendar cal = Calendar.getInstance();
		try {
			cal.setTime(df.parse(date));
			return cal;
		} catch (Exception e) {
			return null;
		}		
	}	
	
	public static DateTime getDateTimeFromSql(String date) {
		SimpleDateFormat df = new SimpleDateFormat(DATE_STANDARD, Locale.getDefault());
		df.setTimeZone(TimeZone.getTimeZone("UTC"));
		try {
			return new DateTime(df.parse(date).getTime());
		} catch (Exception e) {
			return null;
		}		
	}	
	
	public static String getDateForSql(Calendar calendar) {
	    SimpleDateFormat sdf = new SimpleDateFormat(DATE_STANDARD);
	    sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
	    if (calendar == null)
	    	return null;
	    else {
		    String utcTime = sdf.format(calendar.getTime());
		    return utcTime;
	    }
	}
	
	public static String getDateForSql(DateTime date) {
	    final SimpleDateFormat sdf = new SimpleDateFormat(DATE_STANDARD);
	    sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
	    if (date == null)
	    	return null;
	    else {
		    String utcTime = sdf.format(date.toDate());
		    return utcTime;
	    }
	}
	
	public static String getDateForSql(Date date) {
	    final SimpleDateFormat sdf = new SimpleDateFormat(DATE_STANDARD);
	    sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
	    if (date == null)
	    	return null;
	    else {
		    String utcTime = sdf.format(date);
		    return utcTime;
	    }
	}

	public static int getDaysBetween(Calendar dateFrom, Calendar dateTo) {
		return Math.abs((int) ((dateTo.getTimeInMillis() - dateFrom
				.getTimeInMillis()) / (1000 * 60 * 60 * 24)));
	}
	
	public static String getMonthForSql(Calendar date) {
		String month = Integer.toString(date.get(Calendar.MONTH));
		if (month.length() == 1)
			month = "0" + month;
		return month;
	}

	public static Date convertToUtcDate(String StrDate)
	{
	    Date dateToReturn = null;
	    SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_STANDARD);

	    try
	    {
	        dateToReturn = (Date)dateFormat.parse(StrDate);
	    }
	    catch (ParseException e)
	    {
	        e.printStackTrace();
	    }

	    return dateToReturn;
	}
	
	public static Calendar getStartOfTheMonth(Calendar monthDate) {
		LocalDate date = LocalDate.fromCalendarFields(monthDate).withDayOfMonth(1);
		Calendar cal = Calendar.getInstance();
		cal.setTime(date.toDate());
		return cal;
	}
	
	public static Calendar getEndOfTheMonth(Calendar monthDate) {
		LocalDate date = LocalDate.fromCalendarFields(monthDate).plusMonths(1).withDayOfMonth(1).minusDays(1);
		Calendar cal = Calendar.getInstance();
		cal.setTime(date.toDate());
		return cal;
	}
	
	public static String getStartOfTheMonthForSql(Calendar monthDate) {
		return getDateForSql(getStartOfTheMonth(monthDate));
	}
	
	public static String getEndOfTheMonthForSql(Calendar monthDate) {
		return getDateForSql(getEndOfTheMonth(monthDate));
	}
	
	/**
	 * @return yyyy-MM-dd
	 */
	public static SimpleDateFormat getShortDateFormatter() {
		return new SimpleDateFormat(DATE_STANDARD_SHORT, Locale.getDefault());
	}
	
	/**
	 * @return yyyy-MM-dd HH:mm:ss
	 */
	public static SimpleDateFormat getLongDateFormatter() {
		return new SimpleDateFormat(DATE_STANDARD, Locale.getDefault());
	}
	
	/**
	 * @return yyyy-MM-dd HH_mm_ss
	 */
	public static SimpleDateFormat getLongFormatterForFileName() {
		return new SimpleDateFormat(DATE_FOR_FILE_NAME_LONG, Locale.getDefault());
	}
	
	/**
	 * @return yyyy-MM-dd HH:mm:ss
	 */
	public static SimpleDateFormat getMediumDateFormatter() {
		return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
	}
	
	/**
	 * @return yyyy-MM-dd HH:mm
	 */
	public static SimpleDateFormat getNoSecondsFormatter() {
		return new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());
	}

    public static SimpleDateFormat getMonthYearFormatter() {
        return new SimpleDateFormat(DATE_MONTH_YEAR, Locale.getDefault());
    }

    public static SimpleDateFormat getUserFriendlyFormatter() {
        return new SimpleDateFormat(DATE_USER_FRIENDLY, Locale.getDefault());
    }
    
    public static String getDayIndentifier(DateTime dayDate) {
    	StringBuilder sb = new StringBuilder();
    	sb.append(Integer.toString(dayDate.getDayOfMonth())).append("-").append(Integer.toString(dayDate.getMonthOfYear()))
    	.append("-").append(Integer.toString(dayDate.getYear()));
    	return sb.toString();
    }    

    /**
     * Obslugiwane w jezykach: angielski, polski
     * @param date
     * @param capital czy miesiace powinny zaczynac sie duza litera
     * @return Data w formacie 13 czerwiec 2013
     */
    public static String getUserFriendlyDateString(Context context, DateTime date, boolean capital) {
    	StringBuilder sb = new StringBuilder();
    	String[] monthsCapital = context.getResources().getStringArray(R.array.months_capital);
    	String[] months = context.getResources().getStringArray(R.array.months);
    	
    	if (capital) {
    		sb.append(date.getDayOfMonth()).append(" ").append(monthsCapital[date.getMonthOfYear() - 1]).append(" ").append(date.getYear());
    	} else {
    		sb.append(date.getDayOfMonth()).append(" ").append(months[date.getMonthOfYear() - 1]).append(" ").append(date.getYear());
    	}    	
    	
    	return sb.toString();
    }
    
    /**
     * Obslugiwane w jezykach: angielski, polski
     * @param date
     * @param capital czy miesiace powinny zaczynac sie duza litera
     * @return Data w formacie czerwiec 2013
     */
    public static String getUserFriendlyMonthYearString(Context context, DateTime date, boolean capital) {
    	StringBuilder sb = new StringBuilder();
    	String[] monthsCapital = context.getResources().getStringArray(R.array.months_capital);
    	String[] months = context.getResources().getStringArray(R.array.months);
    	
    	if (capital) {
    		sb.append(monthsCapital[date.getMonthOfYear() - 1]).append(" ").append(date.getYear());
    	} else {
    		sb.append(months[date.getMonthOfYear() - 1]).append(" ").append(date.getYear());
    	}    	
    	
    	return sb.toString();
    }
    
    /**
     * Obslugiwane w jezykach: angielski, polski
     * @param date
     * @param capital czy miesiace powinny zaczynac sie duza litera
     * @return Data w formacie czerwiec 2013
     */
    public static String getUserFriendlyDayMonth(Context context, DateTime date, boolean capital) {
    	StringBuilder sb = new StringBuilder();
    	String[] monthsCapital = context.getResources().getStringArray(R.array.months_capital);
    	String[] months = context.getResources().getStringArray(R.array.months);
    	
    	if (capital) {
    		sb.append(date.getDayOfMonth()).append(" ").append(monthsCapital[date.getMonthOfYear() - 1]);
    	} else {
    		sb.append(date.getDayOfMonth()).append(" ").append(months[date.getMonthOfYear() - 1]);
    	}    	
    	
    	return sb.toString();
    }
    
	public static DateTime getNoonDateFromSql(String date) {
		SimpleDateFormat df = new SimpleDateFormat(DATE_STANDARD, Locale.getDefault());
		df.setTimeZone(TimeZone.getTimeZone("UTC"));
		try {
			DateTime noon = new DateTime(df.parse(date).getTime());
			noon = new DateTime(noon.getYear(), noon.getMonthOfYear(), noon.getDayOfMonth(), 12, 0);
			return noon;
		} catch (Exception e) {
			return null;
		}		
	}	
	
	public static String getHourMinutesString(DateTime date) {
		SimpleDateFormat df = new SimpleDateFormat("HH:mm", Locale.getDefault());
		if (date != null) return df.format(date.toDate());
		else return null;
	}
	
	public static String getDateString(String pattern, DateTime date) {
		SimpleDateFormat df = new SimpleDateFormat(pattern, Locale.getDefault());
		return df.format(date.toDate());
	}
	
	/**
	 * @param date
	 * @return Data w formacie 3 Cze
	 */
	public static String getDayMonthDateString(DateTime date) {
		SimpleDateFormat df = new SimpleDateFormat("dd MMM", Locale.getDefault());
		if (date != null) return df.format(date.toDate());
		else return null;
	}
	
	public static int getDaysBetween(DateTime dateFrom, DateTime dateTo) {
		return Math.abs((int) ((dateTo.getMillis() - dateFrom.getMillis()) / (1000 * 60 * 60 * 24)));
	}
	
	public DateTime getCurrentUTCTime() {
		DateTime now = new DateTime(DateTimeZone.UTC);
		return now;
	}
	

}
