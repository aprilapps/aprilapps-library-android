package pl.aprilapps.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

public class AaFileTools {

    public static void saveStringToFile(File file, String string) {
        try {
        	if (file.exists()) file.delete();
        	file.createNewFile();
        	
        	FileWriter out = new FileWriter(file);
        	out.write(string);
        	out.close();
        	
//            FileOutputStream fOut = new FileOutputStream(file);            
//            OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
//            myOutWriter.append(string);
//            myOutWriter.close();
//            fOut.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
	
}
